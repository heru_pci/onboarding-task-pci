# -*- coding: utf-8 -*-
from odoo import http

class Mypage(http.Controller):
    @http.route('/my-page/my-page/', auth='public')
    def index(self, **kw):
        return http.request.render('my-page.index', {
            'teachers': ["Diana Padilla", "Jody Caroll", "Lester Vaughn"],
        })


#     @http.route('/my-page/my-page/objects/', auth='public')
#     def list(self, **kw):
#         return http.request.render('my-page.listing', {
#             'root': '/my-page/my-page',
#             'objects': http.request.env['my-page.my-page'].search([]),
#         })
#     @http.route('/my-page/my-page/objects/<model("my-page.my-page"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('my-page.object', {
#             'object': obj
#         })