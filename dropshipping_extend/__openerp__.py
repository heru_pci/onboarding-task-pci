{
    'name': 'Dropshipping extend',
    'version': '1.0',
    'depends': ['sale'],
    'author': 'Port Cities',
    'description': """
      When the user cancel the SO, the RFQ/PO should also automatically cancelled.
      When the user set the canceled SO to quotation again, the cancelled RFQ/PO should also set to RFQ again
    """,
    'website': 'http://www.portcities.net',
    'category': 'Sales',
    'sequence': 1,
    'auto_install': False,
    'installable': True,
    'application': False,
}
