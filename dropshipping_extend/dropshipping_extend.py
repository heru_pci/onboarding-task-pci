from openerp import models, fields, api, _


class dropshipping_extend(models.Model):
    _inherit = "sale.order"

    @api.multi
    def action_cancel(self):
        res = super(dropshipping_extend, self).action_cancel()
        rfo_obj = self.env['purchase.order']
        # find relational data/object
        search_rfo = rfo_obj.search([('origin','=',self.name)])
        # edit relational data 
        search_rfo.write({'state':'cancel'})
        return res
    

    @api.multi
    def action_draft(self):
        res = super(dropshipping_extend, self).action_draft()
        rfo_obj = self.env['purchase.order']
        # find relational data/object
        search_rfo = rfo_obj.search([('origin','=',self.name)])
        # edit relational data
        search_rfo.write({'state':'draft'})
        return res