from openerp import models, fields, api, _


class purchase_order(models.Model):
	_inherit = "purchase.order"
	
	down_payment = fields.Float('Down_payment')
	total = fields.Float('Total')

	@api.one
	@api.depends('amount_total', 'down_payment')
	def total_full(self):
		a= float(self.amount_total)
		b= float(self.down_payment)
		x = a-b
		self.total = x

	total = fields.Float('Total', compute='total_full')
	cphone = fields.Char('Phone', related='partner_id.phone')