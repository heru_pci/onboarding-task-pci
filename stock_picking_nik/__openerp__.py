{
    'name': 'Stock Picking NIK info',
    'version': '1.0',
    'depends': ['stock'],
    'author': 'Port Cities',
    'description': """
       add information in stock.picking about NIK number each they transfer product.
    """,
    'website': 'http://www.portcities.net',
    'category': 'Purchase',
    'sequence': 1,
    'data':['stock_picking_extend_view.xml'],
    'auto_install': False,
    'installable': True,
    'application': False,
}
