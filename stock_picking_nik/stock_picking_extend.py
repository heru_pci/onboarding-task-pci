from openerp import models, fields, api, _


class stock_picking_type_extends(models.Model):
    _inherit = "stock.picking.type"

    using_nik = fields.Boolean('Using NIK in Stock Picking')



class stock_transfer_details_extends(models.Model):
    _inherit = "stock.transfer_details"
    
    def default_get(self, cr, uid, fields, context):
        res = super(stock_transfer_details_extends, self).default_get(cr, uid, fields, context)
        # finally, found that the picking_ids was set on context by earlier action
        picking_id = context['active_id']
        # because self doesnt own env, so have to use self.pool.get
        picking = self.pool.get('stock.picking')
        # find relational data
        picking_data = picking.browse(cr, uid, picking_id)[0]
        # nah, the problem here is we doesnt have env(this data havent saved either)
        # so cant directly set the field value directly with self.using_nik = blablabla
        # so we use res['using_nik], to pass/set the using_nik value
        res['using_nik'] = picking_data.picking_type_id.using_nik
        return res

    using_nik = fields.Boolean('Using NIK in Stock Picking')
    nik_number = fields.Char('NIK Number')

    @api.one
    def do_detailed_transfer(self):
        res = super(stock_transfer_details_extends, self).do_detailed_transfer()
        # we can directly edit the related data, thanks to api.one
        self.picking_id.write({'nik_number':self.nik_number})
        return res

class stock_picking_extends(models.Model):
    _inherit = "stock.picking"

    nik_number = fields.Char('NIK Number')