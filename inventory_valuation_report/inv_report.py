import time
import cStringIO
import xlsxwriter
from cStringIO import StringIO
import base64
import tempfile
import os
from openerp.osv import fields, osv
from openerp.tools.translate import _
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from xlsxwriter.utility import xl_rowcol_to_cell
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
import logging
import re

_logger = logging.getLogger(__name__)
 

class inventory_valuation_report(osv.osv_memory):
    _name = "inventory.valuation.report"
    _description = "Inventory Valuation Report"
    
    _columns = {
        'location_id': fields.many2one('stock.location', 'Location'),
        'product_category' : fields.many2one('product.category', 'Product Category'), 
        'inv_date': fields.datetime('Date Time'),
        'state_x': fields.selection( ( ('choose','choose'),('get','get'),)), #xls
        'data_x': fields.binary('File', readonly=True),
        'name': fields.char('Filename', 100, readonly=True),
        'all_location': fields.boolean('All location')
    }
    
    _defaults = {
        'state_x': lambda *a: 'choose',
    }
    

  
    def excel_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        data = {}
        return self._print_excel_report(cr, uid, ids, data, context=context)
    
    def _print_excel_report(self, cr, uid, ids, data, context=None): 
        inv_report_obj = self.browse(cr, uid, ids, context=context)
        inv_date = inv_report_obj.inv_date              
        location_id = False
        location_name = False
        product_category = False
        product_name = "All"
        dummy_date = datetime.strptime(inv_date, DEFAULT_SERVER_DATETIME_FORMAT)
        dummy_date1 = datetime.strftime(dummy_date, '%d %m %Y')

        if inv_report_obj.location_id:
            location_id = inv_report_obj.location_id.id
            location_name = inv_report_obj.location_id.complete_name
        
        if inv_report_obj.product_category:
            product_category = inv_report_obj.product_category.id
            product = inv_report_obj.product_category
            product_name = product.name
            parent_id = product.parent_id

            while(parent_id):                
                product_name = parent_id.name +" / "+product_name
                parent_id    = parent_id.parent_id            

        fp = StringIO()
        workbook = xlsxwriter.Workbook(fp)
        filename = 'Inventory_Report_'+ dummy_date1 + '.xlsx'
        
        #### STYLE
        #################################################################################
        top_style = workbook.add_format({'bold': 1,'valign':'vcenter'})
        top_style.set_font_name('Arial')
        top_style.set_font_size('15')
        top_style.set_text_wrap()
        #################################################################################
        top_style2 = workbook.add_format({'bold': 1,'valign':'vcenter'})
        top_style2.set_font_name('Arial')
        top_style2.set_font_size('12')
        top_style2.set_text_wrap()
        #################################################################################
        header_style = workbook.add_format({'bold': 1,'align':'center','valign':'vcenter'})
        header_style.set_font_name('Arial')
        header_style.set_font_size('11')
        header_style.set_border()
        header_style.set_text_wrap()
        header_style.set_bg_color('#BDC3C7')
        #################################################################################
        normal_style = workbook.add_format({'valign':'vcenter'})
        normal_style.set_border()
        normal_style.set_text_wrap()
        normal_style.set_font_name('Arial')
        normal_style.set_font_size('11')
        #################################################################################
        normal_center = workbook.add_format({'valign':'vcenter','align':'center'})
        normal_center.set_border()
        normal_center.set_text_wrap()
        normal_center.set_font_name('Arial')
        normal_center.set_font_size('11')
        #################################################################################
        normal_float = workbook.add_format({'valign':'vcenter','align':'left'})
        normal_float.set_border()
        normal_float.set_text_wrap()
        normal_float.set_num_format('#,##0.00;-#,##0.00')
        normal_float.set_font_name('Arial')
        normal_float.set_font_size('11')
        #################################################################################
        tot_style = workbook.add_format({'bold': 1,'align':'left','valign':'vcenter'})
        tot_style.set_font_name('Arial')
        tot_style.set_font_size('12')
        tot_style.set_border()
        tot_style.set_text_wrap()       
        tot_style.set_bg_color('#BDC3C7')
        #################################################################################
        
        worksheet = workbook.add_worksheet("Inventory")        
        # if all_location:
        #     location_ids = self.pool.get('stock.location').search([])
        if location_id:
            worksheet.set_column('A:A', 60)
            worksheet.set_column('B:B', 15) 
            worksheet.set_column('C:C', 70) 
            worksheet.set_column('D:D', 15)
            worksheet.set_column('E:E', 20)
            worksheet.set_column('F:F', 40)
            
            worksheet.merge_range('A1:D1', 'Inventory Valuation Report', top_style)
            worksheet.merge_range('A2:D2', 'Date : ' + dummy_date1, top_style2)
            worksheet.merge_range('A3:D3', 'Product Category : ' + product_name, top_style2)
            worksheet.merge_range('A4:D4', 'Location : ' + location_name, top_style2)
            
            worksheet.set_row(0, 30)
            worksheet.set_row(5, 25)
            worksheet.write(5, 0, 'Category Information', header_style)
            worksheet.write(5, 1, 'Product Code', header_style)
            worksheet.write(5, 2, 'Product Name', header_style)
            worksheet.write(5, 3, 'Quantity', header_style)
            worksheet.write(5, 4, 'Amount', header_style)
            worksheet.write(5, 5, 'Category Information', header_style)
        else:
            worksheet.set_column('A:A', 30)
            worksheet.set_column('B:B', 15) 
            worksheet.set_column('C:C', 70) 
            worksheet.set_column('D:D', 15)
            worksheet.set_column('E:E', 20)
            worksheet.set_column('F:F', 60)
            
            worksheet.merge_range('A1:E1', 'Inventory Valuation Report', top_style)
            worksheet.merge_range('A2:E2', 'Date : ' + dummy_date1, top_style2)
            worksheet.merge_range('A3:E3', 'Product Category : ', top_style2)
            worksheet.merge_range('A4:E4', 'Location : All Internal Locations', top_style2)
            
            worksheet.set_row(0, 30)
            worksheet.set_row(5, 25)
            worksheet.write(5, 0, 'Category Information', header_style)
            worksheet.write(5, 1, 'Product Code', header_style)
            worksheet.write(5, 2, 'Product Name', header_style)
            worksheet.write(5, 3, 'Quantity', header_style)
            worksheet.write(5, 4, 'Amount', header_style)
            worksheet.write(5, 5, 'Location Information', header_style)

        product_src = self.pool.get('product.product').search(cr, uid, [('active','=',True)], order='id ASC') 
        product_obj = self.pool.get('product.product').browse(cr, uid, product_src, context=context)
        
        #### to get set list (not duplicate) of category, for filling total qty and amount total per category ####
        term_categ_id = []
        for product in product_obj:             
            category_name = product.categ_id.name
            parent_id = product.categ_id.parent_id

            """ to get product category name"""
            while(parent_id):
                category_name = parent_id.name +" / "+ category_name
                parent_id     = parent_id.parent_id            

            if product_name not in category_name:
                continue 
            term_categ_id.append(str(category_name))            
        set_term_categ_id = set(term_categ_id)
        list_term_categ_id = list(set_term_categ_id)
        
        #start_row used for filling total qty and amount total per category
        start_row = 6
        ###############################################################################################################

        rows = 6
        grand_tot = 0
        grand_amount = 0
        for categ_vals in list_term_categ_id:
            rows_tot = 0
            for product in product_obj:             
                category_name = product.categ_id.name
                parent_id = product.categ_id.parent_id
                  
                """ to get product category name"""
                while(parent_id):
                    category_name = parent_id.name +" / "+ category_name
                    parent_id     = parent_id.parent_id            
                                      
                if product_name not in category_name:
                    continue
                  
                if categ_vals == category_name:
                    if location_id:
                        tq = []                
                        inv_qty = 0
                        cr.execute("select sum(qty) from stock_quant where product_id = %s and location_id = %s",(product.id,location_id))
                        quants = cr.fetchone()[0]
                        if quants:
                            inv_qty += quants   
                            grand_tot += inv_qty    
                            
                            sum_am = inv_qty * product.standard_price
                            grand_amount += sum_am    
                          
                        cr.execute("select sum(product_uom_qty) from stock_move where product_id = %s and location_dest_id = %s and date > %s and state = %s",(product.id,location_id,inv_date,'done'))
                        moves_in = cr.fetchone()[0]
                        if moves_in:
                            inv_qty -= moves_in
                          
                        cr.execute("select sum(product_uom_qty) from stock_move where product_id = %s and location_id = %s and date > %s and state = %s",(product.id,location_id,inv_date,'done'))
                        moves_out = cr.fetchone()[0]
                        if moves_out:
                            inv_qty += moves_out
                          
                        worksheet.write(rows, 0, category_name, normal_style)
                        worksheet.write(rows, 1, product.default_code or '', normal_center)
                        worksheet.write(rows, 2, product.name_template, normal_style)
                        worksheet.write(rows, 3, inv_qty, normal_float)
                        worksheet.write(rows, 4, inv_qty * product.standard_price, normal_float)
                        worksheet.write(rows, 5, location_name, normal_style)
                        rows += 1
                        rows_tot += 1    
                    else:
                        location_src = self.pool.get('stock.location').search(cr, uid, [('usage','=','internal')], order='id ASC')
                        i = 1
                        for location in self.pool.get('stock.location').browse(cr, uid, location_src, context=context):
                            rows_tot += 1
                            inv_qty = 0
                            cr.execute("select sum(qty) from stock_quant where product_id = %s and location_id = %s",(product.id,location.id))
                            quants = cr.fetchone()[0]
                            if quants:
                                inv_qty += quants
                              
                            cr.execute("select sum(product_uom_qty) from stock_move where product_id = %s and location_dest_id = %s and date > %s and state = %s",(product.id,location.id,inv_date,'done'))
                            moves_in = cr.fetchone()[0]
                            if moves_in:
                                inv_qty -= moves_in
                              
                            cr.execute("select sum(product_uom_qty) from stock_move where product_id = %s and location_id = %s and date > %s and state = %s",(product.id,location.id,inv_date,'done'))
                            moves_out = cr.fetchone()[0]
                            if moves_out:
                                inv_qty += moves_out
                              
                            if i == 1:
                                worksheet.write(rows, 0, category_name, normal_style)
                                worksheet.write(rows, 1, product.default_code or '', normal_center)
                                worksheet.write(rows, 2, product.name_template, normal_style)
                                worksheet.write(rows, 3, inv_qty, normal_float)
                                worksheet.write(rows, 4, inv_qty * product.standard_price, normal_float)
                                worksheet.write(rows, 5, location.location_id.name + '/' + location.name, normal_style)
                                  
                                rows += 1
                                i += 1
                            else:
                                worksheet.write(rows, 0, category_name, normal_style)
                                worksheet.write(rows, 1, '', normal_center)
                                worksheet.write(rows, 2, product.name_template, normal_style)
                                worksheet.write(rows, 3, inv_qty, normal_float)
                                worksheet.write(rows, 4, inv_qty * product.standard_price, normal_float)
                                worksheet.write(rows, 5, location.location_id.name + '/' + location.name, normal_style)
                                  
                                rows += 1                          
                             
            if rows_tot == 6:
                sum_qty = '{=sum(D%s:D%s)}' % (start_row,rows_tot)
                sum_amount = '{=sum(E%s:E%s)}' % (start_row,rows_tot)
            else:
                sum_qty = '{=sum(D%s:D%s)}' % (start_row+1,start_row+rows_tot)
                sum_amount = '{=sum(E%s:E%s)}' % (start_row+1,start_row+rows_tot)
            worksheet.write(rows, 0, "Total", tot_style)
            worksheet.write(rows, 1, "", tot_style)
            worksheet.write(rows, 2, "", tot_style)
            worksheet.write_formula(rows, 3, sum_qty, tot_style)
            worksheet.write_formula(rows, 4, sum_amount, tot_style)            
            worksheet.write(rows, 5, "", tot_style)
            rows+=1
            start_row+=rows_tot+1
            
        worksheet.write(rows, 0, "Grand Total", tot_style)
        worksheet.write(rows, 1, "", tot_style)
        worksheet.write(rows, 2, "", tot_style)
        worksheet.write(rows, 3, grand_tot, tot_style)
        worksheet.write(rows, 4, grand_amount, tot_style)            
        worksheet.write(rows, 5, "", tot_style)
        
        workbook.close()
         
        out=base64.encodestring(fp.getvalue())
        self.write(cr, uid, ids, {'state_x':'get', 'data_x':out, 'name': filename}, context=context)
        ir_model_data = self.pool.get('ir.model.data')
        fp.close()
 
        form_res = ir_model_data.get_object_reference(cr, uid, 'inventory_valuation_report', 'inv_valuation_report_wizard_view')
 
        form_id = form_res and form_res[1] or False
        return {
            'name': _('Download XLS'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'inventory.valuation.report',
            'res_id': ids[0],
            'view_id': False,
            'views': [(form_id, 'form')],
            'type': 'ir.actions.act_window',
            'target': 'current'
        }