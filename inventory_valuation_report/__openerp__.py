{
    'name': 'Inventory Valuation Report', 
    'version' : '1.0',   
    'depends': ["base","stock"],
    'author': 'Port Cities Indonesia',
    'description': """
        
         * v 1.0
         - Generate inventory report based on location and date.
         - Author : PAA
        
         * v 1.1
         - add filter by product category
         - Author : Fachrian LR
         
         * v 1.2
         - change category information in inventory_valuation_report
         - create total and grand total in inventory_valuation_report
         - Author : Alif DArari 
        
    """,
    'website': 'http://www.portcitiesindonesia.com',
    'category': 'Report',
    'sequence': 1,
    'data': ['inv_report_view.xml'],
    'auto_install': False,
    'installable': True,
    'application': False,
}