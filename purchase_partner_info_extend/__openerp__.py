{
    'name': 'Purchase Suplier Info',
    'version': '1.0',
    'depends': ['purchase'],
    'author': 'Port Cities',
    'description': """
        Add supplier email and phone on purchase 
    """,
    'website': 'http://www.portcities.net',
    'category': 'Purchase',
    'sequence': 1,
    'data': ['purchase_view.xml'],
    'auto_install': False,
    'installable': True,
    'application': False,
}
