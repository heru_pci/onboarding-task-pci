from openerp import models, fields, api, _


class purchase_order(models.Model):
	_inherit = "purchase.order"


	supplier_phone = fields.Char('Phone', related='partner_id.phone')
	supplier_email = fields.Char('Email', related='partner_id.email')