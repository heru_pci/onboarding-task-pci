{
    'name': 'Loan Order',
    'version': '1.0',
    'depends': ['sale','product'],
    'author': 'Port Cities',
    'description': """
      Sometimes, when we don't have an item in stock, we loan another item to the customer during the waiting time. 
      But of course, the loan is not record in the SO,
      so we sometimes forget which item we have loaned to which customer.
    """,
    'website': 'http://www.portcities.net',
    'category': 'Loan Order',
    'sequence': 1,
    'data': [
      'security/user_groups.xml',
      'security/ir.model.access.csv',
      'views/loan_order.xml',
      'data/ir_sequence_data.xml'
      ],
    'demo':['data/loan_order_demo.xml'],
    'auto_install': False,
    'installable': True,
    'application': False,
}