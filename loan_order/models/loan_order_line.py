from odoo import models, fields, api
from odoo.tools.translate import _


class LoanOrderLine(models.Model):
    """ The summary line for a class docstring should fit on one line.

    Fields:
      name (Char): Human readable name which will identify each record.

    """
    @api.model
    def create(self, vals):
        if vals.get('product_id'):
            prod_obj = self.env['product.product'].search([('id','=',vals['product_id'])])
            vals['name'] = prod_obj.name
        result = super(LoanOrderLine, self).create(vals)
        return result
        
    @api.onchange('product_id','qty')
    def _onchange_product_id(self):
        if self.product_id:
            self.name ='['+str(self.product_id.default_code)+'] '+str(self.product_id.name)
            if self.product_id.qty_available < self.qty:
                print "=========================================================="
                print str(self.product_id.qty_available) +' ========='+str(self.product_id.name) 
                print "=========================================================="
                warning_mess = {
                            'title': _('Not enough inventory!'),
                            'message' : _('You plan to get %s but you only have %s available!\n') % \
                                (int(self.qty), int(self.product_id.qty_available))
                        }
                self.qty = self.product_id.qty_available
                return {'warning': warning_mess}
        return {}
        
    _name = 'loan.order.line'
    _description = u'LoanOrderLine'

    _rec_name = 'name'
    _order = 'name ASC'
    
    product_id = fields.Many2one('product.product', string='Product', domain=[('sale_ok', '=', True)], change_default=True, ondelete='restrict', required=True)

    name = fields.Text(
        string='Description',
        required=False,
        readonly=False,
        index=False,
        help=False,
        translate=True
    )
    qty = fields.Float(
        string='Qty',
        required=False,
        readonly=False,
        index=False,
        default=0.0,
        digits=(16, 2),
        help=False
    )
    loan_order_id = fields.Many2one(
        string='loan_order_id',
        comodel_name='loan.order',
        ondelete='cascade',
        auto_join=True
    )
    
    qty_free = fields.Float(
        string='Free Stock',
        related='product_id.qty_available'
    )
    
    
    
    

