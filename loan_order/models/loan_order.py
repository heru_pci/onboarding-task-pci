from odoo import models, fields, api
from odoo.tools.translate import _
import datetime

class LoanOrder(models.Model):
    """ The summary line for a class docstring should fit on one line.

    Fields:
      name (Char): Human readable name which will identify each record.

    """

    _name = 'loan.order'
    _description = u'Loan Order'


    name = fields.Char(
        string='LO Number',
        required=True, 
        index=True, 
        copy=False, 
        default=lambda self: _('New')
    )
    partner_id = fields.Many2one(
        string='Customer Name',
        required=True,
        readonly=False,
        index=False,
        default=None,
        help=False,
        comodel_name='res.partner',
        domain=[],
        ondelete='cascade',
        auto_join=False
    )
    order_date = fields.Datetime(
        string='Loan Date',
        required=True,
        readonly=True,
        index=False,
        default= lambda *a:datetime.datetime.now(),
        help=False
    )
    
    starting_date = fields.Date(
        string='Starting Date',
        required=True,
        readonly=False,
        index=False,
        help=False
    )
    end_date = fields.Date(
        string='End Date',
        required=True,
        readonly=False,
        index=False,
        help=False
    )

    status = fields.Selection(
        string='Status',
        required=True,
        readonly=False,
        index=False,
        default=('Draft', 'Draft'),
        help=False,
        # draft, confirm, partially delivered, delivered, partially returned, returned
        selection=[('Draft', 'Draft'),
         ('Confirm', 'Confirm'), 
         ('Partially delivered', 'Partially delivered'),
         ('Delivered', 'Delivered'), 
         ('Partially returned', 'Partially returned'), 
         ('Returned', 'Returned')]
    )

    loan_order_ids = fields.One2many(
        string='Loan Order Ids',
        required=False,
        readonly=False,
        index=False,
        default=None,
        help=False,
        comodel_name='loan.order.line',
        inverse_name='loan_order_id',
        domain=[],
        limit=None
    )
    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('loan.order') or 'New'
        result = super(LoanOrder, self).create(vals)
        return result
        

