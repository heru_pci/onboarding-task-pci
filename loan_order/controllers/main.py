from openerp import http
from openerp.http import request

class Main(http.Controller):
    """ The summary line for a class docstring should fit on one line.
        Routes:
          /some_url: url description
    """
    @http.route('/api/<model_name>',methods=['GET'], type='json', auth='none')
    def books_json(self,model_name,**kwargs):
        print model_name
        records = request.env[model_name.replace('_','.')]\
        .sudo().search([])              #jika gak pake sudo akan muncul error access privilege
        return records.read()
