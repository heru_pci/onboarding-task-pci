{
    'name': 'Customer Service Purchase',
    'version': '1.0',
    'depends': ['purchase'],
    'author': 'Port Cities',
    'description': """
       Add information about user in purchase
    """,
    'website': 'http://www.portcities.net',
    'category': 'Purchase',
    'sequence': 1,
    'data': ['purchase_view.xml'],
    'auto_install': False,
    'installable': True,
    'application': False,
}
