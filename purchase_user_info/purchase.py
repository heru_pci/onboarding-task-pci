from openerp import models, fields, api, _


class purchase_order(models.Model):
	_inherit = "purchase.order"


	contact_person = fields.Many2one('res.users','Contact Person', default=lambda self: self.env.user)