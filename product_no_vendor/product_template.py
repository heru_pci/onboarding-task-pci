from openerp import models, fields, api, _
from openerp.exceptions import UserError


class product_template(models.Model):
	_inherit = "product.template"
	
	@api.model
	def create(self,vals):
		sellers = vals['seller_ids']
		if len(sellers) < 1:
			# ========================= task 10 ========================================
			# raise UserError(_('You should fill in the supplier details, at least one.'))
			# ========================= task 7 =========================================
			sup=self.env['res.partner']		
			# search and browse		
			no_sup = sup.search([('name','=','no supplier')])
			vals['seller_ids'] =  [(0,0, {'name':no_sup.id})]
		return super(product_template, self).create(vals)

	@api.multi
	@api.depends('seller_ids')
	def write(self, vals):
		res = super(product_template, self).write(vals)
		sellers = self.seller_ids
		#to check if it waas create or edit, 
		#cause in create state, 
		#this vals is ignored
		if self.create_date != self.write_date:
			if len(sellers) < 1:
				# ======================== task 10 =========================================
				# raise UserError(_('You should fill in the supplier details, at least one.'))
				# ======================== task 7 ==========================================
				sup=self.env['res.partner']
				# search and browse		
				no_sup = sup.search([('name','=','no supplier')])
				supinfo = self.env['product.supplierinfo']
				supinfo.create({'product_tmpl_id':self.id,'name':no_sup.id})
		return res
	