{
    'name': 'Product check no Supplier',
    'version': '1.0',
    'depends': ['product'],
    'author': 'Port Cities',
    'description': """
      add condition check if seller_ids is empty, 
      create new record in product.supplierinfo with supplier_id is 'no supplier' (task 7)
      show alert (task 10 )
    """,
    'website': 'http://www.portcities.net',
    'category': 'Purchase',
    'sequence': 1,
    'auto_install': False,
    'installable': True,
    'application': False,
}
