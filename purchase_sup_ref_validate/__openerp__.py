{
    'name': 'Purchase Supplier Reference Validate',
    'version': '1.0',
    'depends': ['purchase'],
    'author': 'Port Cities',
    'description': """
       automatically fill “No Supplier Reference” on supplier reference 
       when user edit or create new purchase order but they didn’t fill it
    """,
    'website': 'http://www.portcities.net',
    'category': 'Purchase',
    'sequence': 1,
    'auto_install': False,
    'installable': True,
    'application': False,
}
