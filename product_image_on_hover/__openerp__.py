{
    'name': 'Product image on hover',
    'version': '1.0',
    'depends': ['web'],
    'author': 'Port Cities',
    'description': """
        Product image on hover
    """,
    'website': 'http://www.portcities.net',
    'category': 'Purchase',
    'sequence': 1,
    'js': 'static/src/js/product_image_on_hover.js',
    'data': ['product_image_on_hover.xml'],
    'auto_install': False,
    'installable': True,
    'application': False,
}
