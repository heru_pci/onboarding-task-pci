{
    'name': 'Customer Invoice Report', 
    'version' : '1.0',   
    'depends': ["base","account"],
    'author': 'Port Cities Indonesia',
    'description': """
        
         * v 1.0
         - This is custom module to learn simple logic python and how to create xlsx report within odoo
         - Author : aziz@portcities.net
        
    """,
    'website': 'http://www.portcitiesindonesia.com',
    'category': 'Report',
    'sequence': 1,
    'data': [
             'views/customer_invoice_report_view.xml'
    ],
    'auto_install': False,
    'installable': True,
    'application': False,
}