import xlsxwriter
from cStringIO import StringIO
import base64
from openerp import models, fields, api, _
from datetime import datetime
import datetime
 

class CustomerInvoiceReport(models.Model):
    _name = "customer.invoice.report"
    # _description = "Customer Invoice Report"

    partner_id = fields.Many2one('res.partner', 'Customer') 
    inv_date = fields.Datetime('Date Time', default = datetime.datetime.now())
    state_x = fields.Selection( ( ('choose','choose'),('get','get'),)) #xls
    data_x = fields.Binary('File', readonly=True)
    name = fields.Char('Filename', readonly=True)
    all_customers = fields.Boolean('All Customer')
     
    # _defaults = {
    #     'state_x': lambda *a: 'choose',
    # }        
    
    @api.multi
    def excel_report(self):
        if self._context is None:
            self._context = {}
        data = {}
        return self._print_excel_report()

    @api.multi
    def _print_excel_report(self): 
        cus_inv_report_obj = self
        fp = StringIO()
        workbook = xlsxwriter.Workbook(fp)
        filename = 'Inventory_Report_'+ cus_inv_report_obj.inv_date + '.xlsx'
        # fix 'this model has no attribute .env'
        # self.env = api.Environment(cr, 1, {}) 
        

        #### STYLE
        #################################################################################
        top_style = workbook.add_format({'bold': 1,'valign':'vcenter'})
        top_style.set_font_name('Arial')
        top_style.set_font_size('15')
        top_style.set_text_wrap()
        #################################################################################
        top_style2 = workbook.add_format({'bold': 1,'valign':'vcenter'})
        top_style2.set_bg_color('#000080')
        top_style2.set_font_name('Arial')
        top_style2.set_border()
        top_style2.set_font_size('11')
        top_style2.set_text_wrap()
        #################################################################################
        header_style = workbook.add_format({'bold': 1,'align':'center','valign':'vcenter'})
        header_style.set_font_name('Arial')
        header_style.set_font_size('11')
        header_style.set_border()
        header_style.set_text_wrap()
        header_style.set_bg_color('#BDC3C7')
        #################################################################################
        normal_style = workbook.add_format({'align':'justify'})
        normal_style.set_border()
        normal_style.set_text_wrap()
        normal_style.set_font_name('Arial')
        normal_style.set_font_size('11')
        #################################################################################
        normal_center = workbook.add_format({'valign':'vcenter','align':'center'})
        normal_center.set_border()
        normal_center.set_text_wrap()
        normal_center.set_font_name('Arial')
        normal_center.set_font_size('11')
        #################################################################################
        normal_float = workbook.add_format({'valign':'vcenter','align':'center'})
        normal_float.set_border()
        normal_float.set_text_wrap()
        normal_float.set_num_format('#,##0.00;-#,##0.00')
        normal_float.set_font_name('Arial')
        normal_float.set_font_size('11')
        #################################################################################
        tot_style = workbook.add_format({'bold': 1,'align':'center','valign':'vcenter'})
        tot_style.set_font_name('Arial')
        tot_style.set_font_size('11')
        tot_style.set_border()
        tot_style.set_text_wrap()
        tot_style.set_bg_color('#BDC3C7')
        #################################################################################
        merge_formats = workbook.add_format({'bold': 1,'align': 'left','valign': 'vcenter',})
        merge_formats.set_font_name('Arial')
        merge_formats.set_font_size('12')
        #################################################################################
        worksheet = workbook.add_worksheet("Customer Invoice") 
        worksheet.set_column('A:A', 20)
        worksheet.set_column('B:B', 20) 
        worksheet.set_column('C:C', 40) 
        worksheet.set_column('D:D', 20)
        worksheet.set_column('E:E', 20)
        worksheet.set_column('F:F', 20)
        worksheet.set_column('G:G', 20)
        #################################################################################
        date_format = workbook.add_format({'num_format': 'dd/mm/yy'})
        date_format.set_font_name('Arial')
        date_format.set_font_size('12')

        worksheet.merge_range('A1:C1', 'Customer Invoice', merge_formats)
        worksheet.write('A2', 'Date', merge_formats)
        date = datetime.datetime.strptime (str(cus_inv_report_obj.inv_date), "%Y-%m-%d %H:%M:%S").strftime('%d/\%m/\%Y')
        worksheet.write('B2', datetime.date.today(), date_format)

        worksheet.write('A4','Invoice Number', header_style)
        worksheet.write('B4','Customer Name', header_style)
        worksheet.write('C4','Product', header_style)
        worksheet.write('D4','Qty', header_style)
        worksheet.write('E4','Price', header_style)
        worksheet.write('F4','Amount', header_style)
        worksheet.write('G4','Invoice Date', header_style)
        

        invoice_obj = self.env['account.invoice']
        i = 5 
        gtqty = 0
        gtsubtotal = 0
        gtprice = 0
        if cus_inv_report_obj.partner_id and cus_inv_report_obj.all_customers==False:
            invoices = invoice_obj.search([('partner_id','=',cus_inv_report_obj.partner_id.id),('date_invoice','>=',cus_inv_report_obj.inv_date)])
        else:
            invoices = invoice_obj.search([('date_invoice','>=',cus_inv_report_obj.inv_date)])
        for invoice in invoices:
            worksheet.write('A'+str(i),invoice.number, normal_style)
            worksheet.write('B'+str(i),invoice.partner_id.name, normal_style)
            worksheet.write('G'+str(i),invoice.date_invoice, normal_style)
            tqty = 0
            tsubtotal = 0
            tprice = 0
            a = 0
            for inv_line in invoice.invoice_line_ids:
                worksheet.write('C'+str(i),inv_line.name, normal_style)
                worksheet.write('D'+str(i),float(inv_line.quantity), normal_float)
                worksheet.write('E'+str(i),'{:,.2f}'.format(inv_line.price_unit), normal_float)
                worksheet.write('F'+str(i),'{:,.2f}'.format(inv_line.price_subtotal), normal_float)
                if a>0:
                    worksheet.write('G'+str(i),'   ', normal_style)
                i=i+1
                a=a+1
                tsubtotal = tsubtotal+ float(inv_line.price_subtotal)
                tqty = tqty + float(inv_line.quantity)
                tprice = tprice + float(inv_line.price_unit)
            if tqty > 0:
                worksheet.merge_range('A'+str(i)+':C'+str(i), 'Total', tot_style)
                worksheet.write('D'+str(i), tqty, tot_style)
                worksheet.write('E'+str(i),'{:,.2f}'.format(tprice), tot_style)
                worksheet.write('F'+str(i),'{:,.2f}'.format(tsubtotal), tot_style)
                worksheet.write('G'+str(i),'  ', tot_style)
                gtqty = gtqty + tqty
                gtprice = gtprice + tprice
                gtsubtotal = gtsubtotal + tsubtotal
                i=i+1
        worksheet.merge_range('A'+str(i)+':C'+str(i), 'Grand Total', tot_style)
        worksheet.write('D'+str(i), gtqty, tot_style)
        worksheet.write('E'+str(i),'{:,.2f}'.format(gtprice), tot_style)
        worksheet.write('F'+str(i),'{:,.2f}'.format(gtsubtotal), tot_style)
        worksheet.write('G'+str(i),'  ', tot_style)
        
        workbook.close() 
        out=base64.encodestring(fp.getvalue())
        self.write({'state_x':'get', 'data_x':out, 'name': filename})
        ir_model_data = self.env['ir.model.data']
        fp.close()

        form_res = self.env.ref('customer_invoice_report.customer_invoice_report_wizard_view')
        print form_res
        # form_res = ir_model_data.get_object_reference(cr, uid, 'customer_invoice_report', 'customer_invoice_report_wizard_view')

  
        form_id = form_res and form_res.id or False
        return {
            'name': _('Download XLS'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'customer.invoice.report',
            'res_id': self.id,
            'view_id': False,
            'views': [(form_id, 'form')],
            'type': 'ir.actions.act_window',
            'target': 'current',
            'clear_breadcrumb': True,
        }