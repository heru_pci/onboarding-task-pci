{
    'name': 'Order date vs expected date counter',
    'version': '1.0',
    'depends': ['purchase'],
    'author': 'Port Cities',
    'description': """
        Calculate how many days (duration) between order date and expected date of purchase order.
    """,
    'website': 'http://www.portcities.net',
    'category': 'Purchase',
    'sequence': 1,
    'data': ['purchase_view.xml'],
    'auto_install': False,
    'installable': True,
    'application': False,
}
