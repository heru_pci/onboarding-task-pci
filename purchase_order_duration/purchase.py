from openerp import models, fields, api, _
from datetime import datetime

class purchase_order(models.Model):
	_inherit = "purchase.order"
	@api.one
	@api.depends('date_order', 'date_planned')
	def compute_duration(self):
		if not self.date_planned:			
			self.date_planned = fields.Date.today()
		d1=datetime.strptime(str(self.date_order[:10]),'%Y-%m-%d') 
		d2=datetime.strptime(str(self.date_planned[:10]),'%Y-%m-%d')
		d3=d2-d1
		self.duration = d3.days


	duration = fields.Float('Duration', compute='compute_duration')