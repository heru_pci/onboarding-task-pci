{
    'name': 'Create Product Tag Report',
    'version': '1.0',
    'depends': ['product'],
    'author': 'Port Cities',
    'description': """
        report that will be shown in menu product to generate product tag report 
    """,
    'website': 'http://www.portcities.net',
    'category': 'Purchase',
    'sequence': 1,
    'js': 'static/src/js/create_button.js',
    'data': ['product_report.xml',
        'views/report_product_templatelabel.xml'],
    'auto_install': False,
    'installable': True,
    'application': False,
}
