{
    'name': 'Replace text create button',
    'version': '1.0',
    'depends': ['web'],
    'author': 'Port Cities',
    'description': """
        Replace text on create button
    """,
    'website': 'http://www.portcities.net',
    'category': 'Purchase',
    'sequence': 1,
    'js': 'static/src/js/create_button.js',
    'data': ['create_button.xml'],
    'qweb': ['static/src/xml/create_button.xml'],   
    'auto_install': False,
    'installable': True,
    'application': False,
}
