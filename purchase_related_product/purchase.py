from openerp import models, fields, api, _


class purchase_order(models.Model):
	_inherit = "purchase.order"

	@api.multi
	def action_view_related_products(self):
		# get list product order line
		order_id = self.order_line
		# create array, we fill it later with collection of product_id
		product_ids=[]
		for line in self.order_line:
			# fill our product id with product id from order line
			product_ids.append(line.product_id.id)
			# set resource
		res = {    	
			'type': 'ir.actions.act_window',    	
			'name': _('Related Products'),    	
			'res_model': 'product.product',    
			# view display: tree, so we only display data, not modify it
			# or you can change it with form, so you can click or add data 	
			'view_type': 'form',    	
			'view_mode': 'tree,form',    	
			'target': 'current',    	
			'domain': [('id','in',product_ids)]    	
		}    	
		return res