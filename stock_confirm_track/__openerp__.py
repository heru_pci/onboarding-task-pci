{
    'name': 'Stock Picking Confirmation Track',
    'version': '1.0',
    'depends': ['stock'],
    'author': 'Port Cities',
    'description': """
       Add information about stock picking confirmation status (mark as todo) and who was confirmed it.
    """,
    'website': 'http://www.portcities.net',
    'category': 'Purchase',
    'sequence': 1,
    'data': ['stock_view.xml'],
    'auto_install': False,
    'installable': True,
    'application': False,
}
