from openerp import models, fields, api, _
from datetime import datetime


class stock(models.Model):
	_inherit = "stock.picking"
	
	@api.multi
	def action_confirm(self, context=None):
		self.confirm_date = datetime.now()
		self.confirm_user = self.env.uid
		return super(stock, self).action_confirm()
	
	

	confirm_date = fields.Datetime('Confirmed Date', readonly=True)
	confirm_user = fields.Many2one('res.users','Confirmed By', default=lambda self: self.env.user, readonly=True)